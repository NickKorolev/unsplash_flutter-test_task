import 'package:flutter/material.dart';
import 'package:flutterapp/model.dart';
import 'package:flutterapp/unsplash_gallery_detailed.dart';

import 'services.dart';

class UnsplashGalleryList extends StatelessWidget {
  final List<UnsplashModel> photos;

  UnsplashGalleryList({this.photos});

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        mainAxisSpacing: 20.0,
        crossAxisSpacing: 20.0,
        crossAxisCount: 2,
      ),
      itemCount: photos.length,
      itemBuilder: (context, index) {
        return GestureDetector(
          onTap: () {
            Navigator.of(context).push(
              MaterialPageRoute<void>(
                builder: (context) => UnsplashGalleryDetailed(
                  nameAuthor: photos[index].nameAuthor,
                  description: photos[index].description,
                  altDescription: photos[index].altDescription,
                  fullImageUrl: photos[index].fullImageUrl,
                ),
              ),
            );
          },
          child: Stack(
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(bottom: 20),
                width: MediaQuery.of(context).size.width / 2 - 20,
                height: 450,
                child: Image.network(photos[index].thumbnailImageUrl,
                    fit: BoxFit.cover),
              ),
              Align(
                  alignment: Alignment.bottomRight,
                  child: Text(
                    photos[index].nameAuthor.toUpperCase(),
                    style: TextStyle(
                      fontSize: 14,
                    ),
                    maxLines: 1,
                  )),
              Align(
                  alignment: Alignment.topLeft,
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.all(5),
                    decoration: BoxDecoration(color: Colors.black54),
                    child: Text(
                      photos[index].description != null
                          ? capitalize(photos[index].description)
                          : photos[index].altDescription != null
                              ? capitalize(photos[index].altDescription)
                              : 'Without description',
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: Colors.white),
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      textDirection: TextDirection.rtl,
                    ),
                  )),
            ],
          ),
        );
      },
    );
  }
}
