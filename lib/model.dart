class UnsplashModel {
  final String nameAuthor;
  final String thumbnailImageUrl;
  final String fullImageUrl;
  final String description;
  final String altDescription;

  UnsplashModel(
      {this.nameAuthor,
      this.thumbnailImageUrl,
      this.fullImageUrl,
      this.description,
      this.altDescription});

  factory UnsplashModel.fromJson(Map<String, dynamic> json) {
    return UnsplashModel(
      nameAuthor: json['user']['name'] as String,
      thumbnailImageUrl: json['urls']['thumb'] as String,
      description: json['description'] as String,
      altDescription: json['alt_description'] as String,
      fullImageUrl: json['urls']['full'] as String,
    );
  }
}
