import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;

import 'model.dart';

Future<List<UnsplashModel>> fetchPhotos(http.Client client) async {
  final response = await client.get(
      'https://api.unsplash.com/photos/?client_id=-RfqZ3BOFrHubgvkzywi6aqU12z6lDzhDxpIVdzZNtU');
  return compute(parsePhotos, response.body);
}

List<UnsplashModel> parsePhotos(String responseBody) {
  final parsed = jsonDecode(responseBody).cast<Map<String, dynamic>>();

  return parsed
      .map<UnsplashModel>((json) => UnsplashModel.fromJson(json))
      .toList();
}

String capitalize(String string) {
  if (string == null) {
    throw ArgumentError("string: $string");
  }

  if (string.isEmpty) {
    return string;
  }

  return string[0].toUpperCase() + string.substring(1);
}
