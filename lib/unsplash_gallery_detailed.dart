import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'model.dart';

class UnsplashGalleryDetailed extends StatelessWidget {
  final String nameAuthor;
  final String fullImageUrl;
  final String description;
  final String altDescription;
  final List<UnsplashModel> photos;

  UnsplashGalleryDetailed(
      {this.nameAuthor,
      this.fullImageUrl,
      this.description,
      this.altDescription,
      this.photos});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios, color: Colors.black),
          onPressed: () => Navigator.pop(context),
        ),
        centerTitle: true,
        title: Text(
          'Author: $nameAuthor',
          style: TextStyle(color: Colors.black),
        ),
        iconTheme: IconThemeData(
          color: Colors.black,
          size: 24.0,
        ),
        backgroundColor: Colors.white,
        elevation: 0,
      ),
      body: Stack(
        children: <Widget>[
          //ImageLoadingBuilder(),
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: Image.network(
              '$fullImageUrl',
              fit: BoxFit.cover,
              loadingBuilder: (BuildContext context, Widget child,
                  ImageChunkEvent loadingProgress) {
                if (loadingProgress == null) return child;
                return Center(
                  child: CircularProgressIndicator(
                    backgroundColor: Color.fromRGBO(46, 139, 87, 1),
                    value: loadingProgress.expectedTotalBytes != null
                        ? loadingProgress.cumulativeBytesLoaded /
                            loadingProgress.expectedTotalBytes
                        : null,
                        
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}