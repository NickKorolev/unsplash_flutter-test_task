
import 'package:flutter/material.dart';
import 'package:flutterapp/services.dart';
import 'package:flutterapp/unsplash_gallery_list.dart';
import 'package:http/http.dart' as http;

import 'model.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final appTitle = 'Unsplash test task';

    return MaterialApp(
      theme: ThemeData(accentColor: Colors.black),
      title: appTitle,
      home: MyHomePage(title: appTitle),
    );
  }
}

class MyHomePage extends StatelessWidget {
  final String title;

  MyHomePage({this.title});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Image.network('https://devsteam.mobi/images/logo.png' , scale:2.5),
        title: Text(title ,  style: TextStyle(color: Colors.black),),
       backgroundColor: Colors.white,
      ),
      body: FutureBuilder<List<UnsplashModel>>(
        future: fetchPhotos(http.Client()),
        builder: (context, snapshot) {
          if (snapshot.hasError) print(snapshot.error);
          return snapshot.hasData
              ? Container(
                  margin: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                  child: UnsplashGalleryList(photos: snapshot.data))
              : Center(child: CircularProgressIndicator());
        },
      ),
    );
  }
}


